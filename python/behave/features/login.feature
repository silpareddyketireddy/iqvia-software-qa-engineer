Feature: Login
  In order to use the app the user must be able to Login

  Scenario: Login Success
    Given the user has the correct credentials
    When the user enters username "test@drugdev.com"
    And the user enters password "supers3cret"
    And clicks Login
    Then the user is presented with a welcome message "Welcome Dr I Test"

  Scenario Outline: Login Incorrect credentials
    Given the user has the incorrect credentials
    When the user enters username '<username>'
    And the user enters password '<password>'
    And clicks Login
    Then the user is presented with a error message '<errormsg>'

    Examples:
      | username          | password           |  errormsg      |
      | wrong-username    |  supers3cret       |  Credentials are incorrect|
      | test@drugdev.com  | wrong-password     |  Credentials are incorrect|
      |   empty-scape     | empty-space        | Credentials are incorrect|
