# -*- coding: utf-8 -*-

from behave import *
from selenium.webdriver.common.by import By
from selenium.webdriver.support import wait


@given(u'the user has the correct credentials')
def step_impl(context):
    context.browser.get('https://sprinkle-burn.glitch.me/')
    # wait.until(presence((By.NAME, 'email')))


@when(u'the enter username "{user_name}"')
def step_imp(context, user_name):
    context.browser.find_element(By.NAME, "email").send_keys(user_name)


@when(u'the user enters password "{password}"')
def step_impl(context, password):
    context.browser.find_element(By.NAME, "password").send_keys(password)


@when(u'clicks Login')
def step_impl(context):
    context.browser.find_element(By.XPATH, "//button[text()='Login']").click()


@then(u'the user is presented with a welcome message "{welcome_msg}"')
def step_impl(context, welcome_msg):
        titles = context.driver.find_elements(By.XPATH, "/html/body/article")
        for t in titles:
            if t.getText() == welcome_msg:
                print("Login successful!\n")
                break


@given(u'the user has the incorrect credentials')
def step_impl(context):
    context.browser.get('https://sprinkle-burn.glitch.me/')
    # wait.until(presence((By.NAME, 'email')))


@then(u'the user is presented with a error message "{err_msg}"')
def step_impl(context, err_msg):
        actual_error_msg = context.driver.find_elements(By.ID, "login-error-box")
        for m in actual_error_msg:
            if m.getText().contains(err_msg):
                print("User doesn't exist or wrong password!\n")
                break


